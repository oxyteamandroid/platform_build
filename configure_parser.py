# -*- coding:utf-8 -*-
"""
* User:   chengjin.wang@ingenic.com
* Date:   15-07-05
* Desc:   Determin what should be built into the system by parsing configuration file.
* Input:  *.xml
* Output: base.mk
"""
import sys
import os
from  xml.dom import  minidom

def get_attrvalue(node, attrname):
     return node.getAttribute(attrname) if node else ''

def get_xmlnode(node,name):
    return node.getElementsByTagName(name) if node else []

def get_package_xml_data(file_name):
    doc = minidom.parse(file_name)
    root = doc.documentElement

    package_nodes = get_xmlnode(root,'package')
    package_list=[]
    for node in package_nodes:
        built_flag = get_attrvalue(node,'build_in')
	if built_flag == 'true':
		node_name = get_attrvalue(node,'packagename')
		package = {}
		package['packagename'] = (node_name)
		package_list.append(package)
        elif built_flag =='false':
                node_name_false = get_attrvalue(node,'packagename')
        else:
                node_name_error = get_attrvalue(node,'packagename')
                print 'ERROR: %s: <build_in> should be true or false'%(node_name_error)
    return package_list

def get_service_xml_data(file_name):
    doc = minidom.parse(file_name)
    root = doc.documentElement

    service_nodes = get_xmlnode(root,'service')
    service_list=[]
    for node in service_nodes:
        built_flag = get_attrvalue(node,'build_in')
	if built_flag == 'true':
		node_name = get_attrvalue(node,'servicename')
		service = {}
		service['servicename'] = (node_name)
		service_list.append(service)
        elif built_flag =='false':
                node_name_false = get_attrvalue(node,'servicename')
        else:
                node_name_error = get_attrvalue(node,'servicename')
                print 'ERROR: %s: <build_in> should be true or false'%(node_name_error)
    return service_list
def load_package_xml():
    package_file='device/ingenic/watch/products/%s/base.mk'% (board_name)
    if len(sys.argv)==1:
        print 'ERROR: Parameters should not be null,Please write the path of the xxx_config.xml.  such as:--product_configure_file=device/ingenic/watch/products/aw808/aw808_config.xml'
    else:
        filename=sys.argv[1]
    package_list = get_package_xml_data(filename)
    f=file(package_file,"w+")
    package_str1=["PRODUCT_PACKAGES += \\\n"]
    f.writelines(package_str1)
    for package in package_list :
        if package:
		package_str2='	%s \\\n'% (package['packagename'])
		f=file(package_file,"a+")
                f.write(package_str2)
		f.close()
def load_service_xml():
    service_file='device/ingenic/watch/products/%s/systemserver.xml'% (board_name)
    if len(sys.argv)==1:
        print 'ERROR: Parameters should not be null,Please write the path of the xxx_config.xml.  such as:--product_configure_file=device/ingenic/watch/products/aw808/config.xml'
    else:
        filename=sys.argv[1]
    service_list = get_service_xml_data(filename)
    f=file(service_file,"w+")
    service_str1='<?xml version="1.0" encoding="utf-8"?>\n'
    service_str2=["<permissions>\n"]
    service_str4=["</permissions>\n"]
    f.writelines(service_str1)
    f.writelines(service_str2)
    for service in service_list :
        if service:
		service_str3='      <feature name="%s\" />\n'% (service['servicename'])
		f=file(service_file,"a+")
                f.write(service_str3)
    f.writelines(service_str4)
    f.close()
if __name__ == "__main__":
    board_name=os.getenv('TARGET_PRODUCT')
    load_package_xml()
    load_service_xml()
