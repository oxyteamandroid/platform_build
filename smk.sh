#!/bin/bash
#
# smk is short for smart make
#
# Copyleft (c) 2013, Duke Fong <duke@dukelec.com>
#

declare -A presets=(
  ["aw808_v11_naturalround_iwop"]="--elf --product_configure_file=device/ingenic/watch/products/aw808/config.xml --uboot=aw808_naturalround_an51_msc0_config --kernel=aw808_naturalround_defconfig --system=aw808-userdebug"
  ["aw808_v11_wisesquare_iwop"]="--elf --product_configure_file=device/ingenic/watch/products/aw808/config.xml --uboot=aw808_wisesquare_an51_msc0_config --kernel=aw808_wisesquare_defconfig --system=aw808-userdebug"
  ["newton2"]="--elf --product_configure_file=device/ingenic/watch/products/newton2/config.xml --uboot=newton2_android51_msc0_config --kernel=newton2_defconfig --system=newton2-userdebug"
  ["solar"]="--elf --product_configure_file=device/ingenic/watch/products/solar/config.xml --uboot=solar_v10_android_msc0_config --kernel=solar_v10_defconfig --system=solar-userdebug"
  # add more here
)

# Multithread compiling
MP=8

# top_dir="`pwd`"
which busybox > /dev/null && BUSYBOX="busybox" # `realpath' not installed by default on many distribution
top_dir="$($BUSYBOX realpath "$(dirname "${BASH_SOURCE[0]}")/..")"

if [ -f "$top_dir/repo" ]; then
  REPO="$top_dir/repo"
elif [ -f ./repo ]; then
  REPO="$(pwd)/repo"
else
  REPO="repo"
fi

toolchain_exist="no"

echoc()
{
  echo -e "\e[0;91m$1\e[0m"
}

usage()
{
  echo -e "\
usage:
  source any/path/to/smk.sh
  smk [<args>]
    support invoke from any location
    double tab for bash auto complete

or:
  any/path/to/smk.sh [<args>]
    direct execution not support bash auto complete

  --clean
    clean up before build
  --stable
    checkout to last tag before build
  -f
    force, no clean up warning

  --elf --elf=
    make the elf project
    --elf       use current version
    --elf=XXX   choice elf version by branch or tag
    --elf=skip  not update elf apk and libs from source

  --product_configure_file --product_configure_file=
    parse configuration file which determines the packages/services to be built into system and generate base.mk for build system.

  --uboot --uboot=
    make uboot
  --kernel --kernel=
    make boot.img
  --system --system=
    make system.img

  --ramdisk
    make ramdisk.img

  --preset=
    make preset board
  -jNUM
    set number of jobs

  --new-board= --brand= --model= --manufacturer=
    change board name, brand name and manufacturer name

  --search=
    search configs
  --list --list=
    show configs if no bash auto complete available
  --help
    this help message
  "
}

envsetup()
{
  echoc "smk: source build/envsetup.sh"
  export CROSS_COMPILE=mips64el-linux-android-
  cd $top_dir
  source build/envsetup.sh
  cd - > /dev/null
  echoc
  echoc "if need help: smk --help"
}
declare -f croot > /dev/null || envsetup

# main function
smk()
{
  local cur_dir="`pwd`"
  local full_args="$@"

  if [ "$1" == "" ]; then
    usage
    return 1
  fi

  local flag_force="no"
  local flag_stable="no"
  local flag_clean="no"

  local board_name=""

  local flag_product="no"
  local flag_elf="no"
  local flag_uboot="no"
  local flag_kernel="no"
  local flag_system="no"
  local conf_elf=""
  local conf_product=""
  local conf_uboot=""
  local conf_kernel=""
  local conf_system=""
  local flag_ramdisk="no"

  local conf_preset=""
  local conf_new_board=""
  local conf_brand=""
  local conf_model=""
  local conf_manufacturer=""

  # options may be followed by one colon to indicate they have a required argument
  if ! options=$(getopt -o j:,f -l stable,clean \
      -l elf::,uboot::,kernel::,system::,product_configure_file::,ramdisk,preset: \
      -l new-board:,brand:,model:,manufacturer:,search:,list::,help -- "$@")
  then
      return 1
  fi

  eval set -- "$options"

  while [ $# -gt 0 ]
  do
      case $1 in
      -f) flag_force="yes" ;;
      -j) MP="`_unquote $2`"; shift ;;
      --stable) flag_stable="yes";;
      --clean) flag_clean="yes";;
      --elf) flag_elf="yes"; conf_elf="`_unquote $2`"; shift ;;
      --product_configure_file) flag_product="yes"; conf_product="`_unquote $2`"; shift ;;
      --uboot) flag_uboot="yes"; conf_uboot="`_unquote $2`"; shift ;;
      --kernel) flag_kernel="yes"; conf_kernel="`_unquote $2`"; shift ;;
      --system) flag_system="yes"; conf_system="`_unquote $2`"; shift ;;
      --ramdisk) flag_ramdisk="yes" ;;
      --preset) conf_preset="`_unquote $2`"; shift ;;
      --new-board) conf_new_board="`_unquote $2`"; shift ;;
      --brand) conf_brand="`_unquote \"$2\"`"; shift ;;
      --model) conf_model="`_unquote \"$2\"`"; shift ;;
      --manufacturer) conf_manufacturer="`_unquote \"$2\"`"; shift ;;
      --search) config_search "`_unquote $2`"; return $? ;;
      --list) config_list "`_unquote $2`"; return $? ;;
      --help) usage; return ;;
      (--) shift; break;;
      (*) echoc "Incorrect parameter: $1"; return 1;;
      esac
      shift
  done

# parse configuration file and generate base.mk before lunch $conf_system
#[ "$flag_product" == "yes" ] && { make_base_mk || { cd "$cur_dir"; return 1; } }

  # expand preset command
  if [ "$conf_preset" != "" ]; then
    full_args=`echo "$full_args" | sed "s/--preset.[^-]*//"`
    local vargs="${presets["`_unquote $conf_preset`"]}"
    vargs="$vargs $full_args"
    echoc "smk: invoke: smk $vargs"
    eval "smk $vargs"
    return $?
  fi

  # clean before build
  if [ "$flag_clean" == "yes" ]; then
    clean_up || { cd "$cur_dir"; return 1; }
  fi
  if [ "$flag_stable" == "yes" ]; then
    checkout_stable || { cd "$cur_dir"; return 1; }
  fi

  # get board_name
  cd "$top_dir"
  if [[ "$conf_kernel" == "" && ! -f kernel-3.10.14/.config ]]; then
    echoc "smk: .config not found for kernel"
    cd "$cur_dir"; return 1;
  fi
  local tmp=""
  [ "$conf_kernel" != "" ] && tmp="kernel-3.10.14/arch/mips/configs/$conf_kernel" || tmp="kernel-3.10.14/.config"
  board_name="`cat $tmp | grep CONFIG_BOARD_NAME`"
  board_name="${board_name%\"}"
  board_name="${board_name#*\"}"
  echoc "smk: board_name: $board_name"
  [ "$board_name" == "" ] && { cd "$cur_dir"; return 1; }

  [ "$conf_kernel" != "" ] && export SMK_KERNEL_CONF=${conf_kernel%_defconfig}_5.1

  # apend "_iwop" to board name for elf project
#  [ "$flag_elf" == "yes" ] && { board_name=${board_name}_iwop; echoc "  change board name to $board_name for elf project"; }

  # new board name
  if [ "$conf_new_board" != "" ]; then
    conf_new_board=`echo $conf_new_board | tr [A-Z] [a-z]`
    echoc "smk: new board name: $conf_new_board"
    [[ ${conf_new_board:0:1} != [a-z] ]] && { echoc "  the first character must be a letter"; return 1; }
    if [[ "$conf_system" == "" || "$conf_kernel" == "" || "$conf_uboot" == "" ]]; then
      echoc "  must set --uboot= &--kernel= & --system="
      cd "$cur_dir"; return 1;
    fi
    #uboot
    #The uboot version must after 2010.09
    echoc "Copy $conf_uboot to create a new configuration item of uboot ${conf_new_board}_android_msc0"
    cd "$top_dir/bootable/bootloader/uboot"
    local source_target=${conf_uboot%"_config"}
    local create_target=${conf_new_board}_android_msc0
    tmp_line=`egrep -i "^[[:space:]]*${source_target}[[:space:]]" boards.cfg`
    if [ "$tmp_line" == "" ]; then
      echo "Create uboot failed not found $source_target in boards.cfg"
      cd "$cur_dir"; return 1;
    fi
    crearte_line=${tmp_line/$source_target/${create_target}}
    sed -i "/$tmp_line/a$crearte_line" boards.cfg
    echoc "Uboot Create $create_target success"

    # kernel
    echoc "  change kernel config BOARD_NAME, you may need to manually copy .config back to arch/mips/configs/XXX_config"
    cd $top_dir
    cd kernel-3.10.14
    make $conf_kernel 2>/dev/null
    sed -i "s/BOARD_NAME=\"${board_name%_iwop}\"/BOARD_NAME=\"${conf_new_board%_iwop}\"/g" .config
    echoc "Copy ${conf_kernel} to arch/mips/configs/${conf_new_board}_defconfig"
    cp .config arch/mips/configs/${conf_new_board}_defconfig -vf
    conf_kernel=""
    # system
    conf_system=`echo "$conf_system" | sed "s/$board_name/$conf_new_board/"`
    copy_board $board_name $conf_new_board
    [ $? -ne 0 ] && { echoc "smk: copy board failed"; cd "$cur_dir"; return 1; }
    board_name="$conf_new_board"
    # just create new board then do not build system
    flag_uboot="no"
    flag_kernel="no"
    flag_system="no"
    flag_elf="no"
  fi

  # pre build the out directory for uboot & kernel copy
  mkdir -p out/target/product/$board_name
  if [ "$conf_system" != "" ]; then
    echoc "smk: lunch $conf_system"
    lunch $conf_system
  elif [[ "$TARGET_PRODUCT" == "" && "$toolchain_exist" == "no" ]]; then
    echoc "  not yet invoke 'lunch', default add gcc toolchain: mipsel-linux-android-4.7"
    PATH=$PATH:$top_dir/prebuilts/gcc/linux-x86/mips/mipsel-linux-android-4.7/bin
    toolchain_exist="yes"
  fi

  # copy key if exist
  if [[ "$flag_elf" == "yes" && -d elf/keystore ]]; then
    echoc "smk: replace platform key/certificate pair"
    cp -vrf elf/keystore/platform.* build/target/product/security/
  fi

  # make all
  echoc "  number of jobs: -j$MP"
  [ "$flag_uboot" == "yes" ] && { make_uboot || { cd "$cur_dir"; return 1; } }
  [ "$flag_ramdisk" == "yes" ] && { make_ramdisk || { cd "$cur_dir"; return 1; } }
  [ "$flag_kernel" == "yes" ] && { make_kernel || { cd "$cur_dir"; return 1; } }
  [ "$flag_product" == "yes" ] && { make_base_mk || { cd "$cur_dir"; return 1; } }
  [ "$flag_system" == "yes" ] && { make_system || { cd "$cur_dir"; return 1; } }
  [[ "$flag_elf" == "yes" && "$conf_elf" != "skip" ]] && { make_elf || { cd "$cur_dir"; return 1; } }

  cd "$cur_dir"
  echoc "smk: successed"
  return 0
}

_unquote()
{
  local tmp="${1%\'}"
  echo "${tmp#\'}"
}

board_path()
{
  find "$top_dir/device" -type d -name "$board_name" | head -1
}

clean_up()
{
  echoc "smk: clean up"
  [ "$REPO" == "" ] && { echo "'repo' script not found"; return 1; }

  if [ "$flag_force" != "yes" ]; then
    echo "This action will remove all the changes and newly created files which are not committed, please back up in advance."
    echo "append -f parameter skip this warning."
    echo ""
    while true; do
      read -p "Do you want to continue? (yes or no) " yn
      case $yn in
          [Yy]* ) break;;
          [Nn]* ) return 1;;
          * ) echo "Please answer yes or no.";;
      esac
    done
  fi

  cd "$top_dir"
  echoc "  clean up..."
  repo forall -c 'git reset --hard > /dev/null || kill $PPID; git clean -dxf > /dev/null || kill $PPID'
  [ $? -ne 0 ] && { echoc "clean up failed"; return 1; }
  rm -rf "$top_dir/out/target"

  return 0
}

checkout_stable()
{
  echoc "smk: checkout stable"
  cd "$top_dir/.repo/manifests"
  last_tag=$(git tag --sort="v:refname" | tail -1)
  [[ "$last_tag" == "" ]] && { echoc "get tag failed"; return 1; }
  echoc "  checkout to last tag: $last_tag"

  cd "$top_dir"
  $REPO forall -c "
    git reset --hard $last_tag > /dev/null || kill \$PPID
  " 2>&1 | sed 's/^/    /'
  [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "reset to tag failed"; return 1; }

  return 0
}

copy_board()
{
  local SOURCE_NAME="$1"
  local DEST_NAME="$2"
  local filename=""

  local SOURCE_NAME_UPPERCASE=`echo $SOURCE_NAME | tr [a-z] [A-Z]`
  local DEST_NAME_UPPERCASE=`echo $DEST_NAME | tr [a-z] [A-Z]`

  cd "$(board_path)/.."
  echoc "  copy $(board_path), pwd: `pwd`"
  rm -rf "$DEST_NAME"
  echoc " copy \"$SOURCE_NAME\" to \"$DEST_NAME\""
  cp -a $SOURCE_NAME $DEST_NAME
  [ $? -ne 0 ] && return 1

  echoc "smk: repace file content"
  find $DEST_NAME -not \( -name .git -prune \) -type f -print0 \
  | while read -r -d $'\0' filename; do
      MIME="$(file -i "$filename"), $(file "$filename")"
      [[ "$MIME" =~ "text" ]] || continue # skip hex file
      # perform the replacement if it is a word
      sed -i ":a; s/\([^a-zA-Z0-9]\|^\)$SOURCE_NAME\([^a-zA-Z0-9]\|$\)/\1$DEST_NAME\2/g; ta" "$filename"
      sed -i ":a; s/\([^a-zA-Z0-9]\|^\)$SOURCE_NAME_UPPERCASE\([^a-zA-Z0-9]\|$\)/\1$DEST_NAME_UPPERCASE\2/g; ta" "$filename"
      # except pattern xxxxFn
      sed -i "s/${SOURCE_NAME}Fn/${DEST_NAME}Fn/g" "$filename"
    done

  echoc "smk: repace filename"
  find $DEST_NAME -not \( -name .git -prune \) -type f -print0 \
  | while read -r -d $'\0' filename; do
      echo "$filename" | grep -q "$SOURCE_NAME" \
      && mv "$filename" "${filename/$SOURCE_NAME/$DEST_NAME}"
    done

  if [ "$conf_manufacturer" != "" ]; then
    sed -i "s/^PRODUCT_MANUFACTURER :=.*/PRODUCT_MANUFACTURER := $conf_manufacturer/" $DEST_NAME/$DEST_NAME.mk
  fi
  if [ "$conf_brand" != "" ]; then
    sed -i "s/^PRODUCT_BRAND :=.*/PRODUCT_BRAND := $conf_brand/" $DEST_NAME/$DEST_NAME.mk
  fi
  if [ "$conf_model" != "" ]; then
    sed -i "s/^PRODUCT_MODEL :=.*/PRODUCT_MODEL := $conf_model/" $DEST_NAME/$DEST_NAME.mk
  fi

  cd "$top_dir"
  return 0
}

#

make_elf()
{
  echoc "smk: make elf $conf_elf"
  cd "$top_dir/elf" || { echoc "$top_dir/elf not exist" > /dev/stderr; return 1; }

  echoc "  delete priv-app"
  cd $top_dir
  rm -rf out/target/product/$board_name/system/app/
  rm -rf out/target/product/$board_name/system/priv-app/
  touch  out/target/product/$board_name/system/build.prop

  cd $top_dir
  export MP

  if [[ -d elf/iwds ]]; then
    echoc "  invoke elf/AntBuildConfig/Antbuild.sh release"
    elf/AntBuildConfig/Antbuild.sh clean   2>&1 | sed 's/^/    /'
    elf/AntBuildConfig/Antbuild.sh release 2>&1 | sed 's/^/    /'
    [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "smk: make elf failed"; return 1; }
  else
    echoc "  invoke elf/AntBuildConfig/elf-release-build.sh release"
    elf/AntBuildConfig/elf-release-build.sh clean 2>&1 | sed 's/^/    /'
    elf/AntBuildConfig/elf-release-build.sh release 2>&1 | sed 's/^/    /'
    [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "smk: make elf failed"; return 1; }
  fi

  echoc "  invoke mmm elf/AmazingSettings/"
  mkdir -p elf/AmazingSettings/libs
  echoc "  replacing elf jar"
  cp -vrf elf/JavaDocAndBuildOut/iwds-api*/user/libs/*   elf/AmazingSettings/libs/.
  mmm elf/AmazingSettings/ 2>&1 | sed 's/^/    /'
  [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "smk: make AmazingSettings failed"; return 1; }

#  echoc "  invoke mmm elf/AmazingMobileCenter/"
#  mkdir -p elf/AmazingMobileCenter/libs
#  echoc "  replacing elf jar"
#  cp -vrf elf/JavaDocAndBuildOut/iwds-api*/user/libs/*   elf/AmazingMobileCenter/libs/.
#  mmm elf/AmazingMobileCenter/ 2>&1 | sed 's/^/    /'
#  [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "smk: make AmazingMobileCenter failed"; return 1; }

  echoc "  replacing elf app , jar & lib ..."
  mkdir -p vendor/ingenic/watchcommon/apps/elf-apps/iwds-so/
  cp -vrf elf/JavaDocAndBuildOut/watchapp/*         vendor/ingenic/watchcommon/apps/elf-apps 
  cp -vrf elf/JavaDocAndBuildOut/iwds-api*/server/libs/mips vendor/ingenic/watchcommon/apps/elf-apps/iwds-so/
  cp -vrf elf/JavaDocAndBuildOut/iwds-api*/user/*  packages/apps/
  cp -vrf elf/JavaDocAndBuildOut/watchmanagerapp/*  out/target/product/$board_name/
  cp -vrf elf/AmazingLauncher/libs/mips/*.so        out/target/product/$board_name/system/lib

  echoc "  making system.img ..."
  make systemimage -j4
  [ $? -ne 0 ] && return 1
  return 0
}
make_base_mk()
{
  if [ "$conf_product" != "" ]; then
   echoc "smk: python configure_parser.py --product_configure_file=$conf_product"
   echoc "smk: update base.mk and systemserver.xml"
  else
   echoc "smk:ERROR:you should not be here,please checkout your product_configure_file Parameters"
  fi
  cd "$top_dir"
  python build/configure_parser.py $conf_product
}
make_uboot()
{
  echoc "smk: make u-boot $conf_uboot"
  cd "$top_dir/bootable/bootloader/uboot"

  make distclean
  make $conf_uboot

  make $1
  make -j$MP
  [ $? -ne 0 ] && { echoc "smk: make uboot failed"; return 1; }

  if [ "$board_name" != "" ]; then
    echoc "smk: coping uboot *.bin"
    cp -vf *.bin ../../../out/target/product/$board_name/
  else
    echoc "smk: skip coping uboot bin, don't know board_name"
  fi
  return 0
}

make_ramdisk()
{
  echoc "smk: make ramdisk"
  cd "$top_dir"
  out/host/linux-x86/bin/mkbootfs out/target/product/$board_name/root | \
    out/host/linux-x86/bin/minigzip > out/target/product/$board_name/ramdisk.img
  [ ${PIPESTATUS[0]} -ne 0 ] && { echoc "smk: make ramdisk failed"; return 1; }
  return 0
}

make_kernel()
{
  echoc "smk: make kernel $conf_kernel"
  cd "$top_dir/kernel-3.10.14"

  [ "$conf_kernel" != "" ] &&  make $conf_kernel
  make zImage -j$MP
  [ $? -ne 0 ] && { echoc "smk: make kernel failed"; return 1; }
  echoc "smk: coping zImage"
  cp -vf arch/mips/boot/compressed/zImage "$(board_path)/kernel"
  cp -vf arch/mips/boot/compressed/zImage "$(board_path)/kernel_recovery"

  cd ..
  if [ ! -f out/target/product/$board_name/ramdisk.img ]; then
    echoc "smk: skip package boot.img because ramdisk.img was not found"
    return 0
  fi
  echoc "smk: update boot.img"
  out/host/linux-x86/bin/mkbootimg --kernel kernel-3.10.14/arch/mips/boot/compressed/zImage \
    --ramdisk out/target/product/$board_name/ramdisk.img \
    --output out/target/product/$board_name/boot.img
  [ $? -ne 0 ] && { echoc "smk: make boot.img failed"; return 1; }
  return 0
}

make_system()
{
  echoc "smk: make system $conf_system"
  cd "$top_dir"
  # echoc "smk: update-api"
  # make update-api -j$MP
  # [ $? -ne 0 ] && { echoc "smk: update-api failed"; return 1; }
  # echoc "smk: making system"
  make -j$MP
  [ $? -ne 0 ] && { echoc "smk: make system failed"; return 1; }


#Now do not build watch manage apk
:<<'NOT_BUILD_WATCH_MANAGE'
  if [ "$flag_elf" == "no" ]; then
    echoc "smk: make IndroidSyncMobile (enter arm temporary environment)"
    mmm frameworks/support/v7/appcompat/
    [ "`cat device/generic/armv7-a-neon/mini_armv7a_neon.mk | grep PRODUCT_LOCALES`" == "" ] && \
      echo "PRODUCT_LOCALES := en_US zh_CN zh_TW en_GB fr_FR it_IT de_DE es_ES cs_CZ ru_RU ko_KR ar_EG ar_IL ja_JP in_ID" \
        >> device/generic/armv7-a-neon/mini_armv7a_neon.mk
    bash -c "
      source build/envsetup.sh
      lunch mini_armv7a_neon-userdebug
      mmm packages/apps/IndroidSyncMobile/
    "
    [ $? -ne 0 ] && { echoc "smk: make IndroidSyncMobile failed"; return 1; }
    cp -v out/target/product/mini_armv7a_neon/IndroidSyncMobile.apk out/target/product/$board_name/
  fi
NOT_BUILD_WATCH_MANAGE
  return 0
}

# get configs
get_config_list()
{
  # $1: uboot, kernel, system or preset
  local cur_dir="`pwd`"
  cd "$top_dir"
  local out=""
  case "$1" in
  elf)
    while read line; do
      [[ "$line" =~ " " ]] || echo "${line}"
    done <<< "$(echo "skip")"
    ;;
  uboot)
    out="`awk '(NF && $1 !~ /^#/){print $1"_config" }'  bootable/bootloader/uboot/boards.cfg`"
    while read -r line; do
      echo "${line%:*}"
    done <<< "$out"
    ;;
  kernel)
    ls kernel-3.10.14/arch/mips/configs/
    ;;
  system)
#  for f in `/bin/ls vendor/*/vendorsetup.sh vendor/*/*/vendorsetup.sh device/*/*/vendorsetup.sh device/*/*/*/*/vendorsetup.sh  2> /dev/null`
for f in `test -d device && find -L device -maxdepth 6 -name 'vendorsetup.sh' 2> /dev/null` \
         `test -d vendor && find -L vendor -maxdepth 4 -name 'vendorsetup.sh' 2> /dev/null`
  do
    out="`cat $f | grep add_lunch_combo`"
    while read -r line; do
      [ "${line%add_lunch_combo*}" != "" ] && continue
      echo ${line#*\ }
    done <<< "$out"
  done
    ;;
  preset) echo "${!presets[@]}" ;;
  list)
    echo "elf"
    echo "uboot"
    echo "kernel"
    echo "system"
    echo "preset"
    ;;
  esac
  cd "$cur_dir"
}

config_search()
{
  echoc "-------- elf configs --------"
  get_config_list elf | grep $1
  echoc

  echoc "-------- uboot configs --------"
  get_config_list uboot | grep $1
  echoc

  echoc "-------- kernel configs --------"
  get_config_list kernel | grep $1
  echoc

  echoc "-------- system configs --------"
  get_config_list system | grep $1
  echoc

  echoc "-------- preset configs --------"
  get_config_list preset | grep $1
  echoc
}

config_list()
{
  if [ "$1" == "" ]; then
    echoc "-------- elf configs --------"
    get_config_list elf | column
    echoc

    echoc "-------- uboot configs --------"
    get_config_list uboot | column
    echoc

    echoc "-------- kernel configs --------"
    get_config_list kernel | column
    echoc

    echoc "-------- system configs --------"
    get_config_list system | column
    echoc

    echoc "-------- preset configs --------"
    get_config_list preset | column
    echoc
  fi
  case "$1" in
  clean|stable) get_config_list "$1" | column ;;
  elf) get_config_list elf | column ;;
  uboot) get_config_list uboot | column ;;
  kernel) get_config_list kernel | column ;;
  system) get_config_list system | column ;;
  preset) get_config_list preset | column ;;
  esac
}

if [ "${0%smk.sh}smk.sh" == "$0" ]; then
  echoc "smk: direct execution"
  smk $@
  exit $?
fi

# auto complete
_smk()
{
  local cur=${COMP_WORDS[COMP_CWORD]}
  local prev=${COMP_WORDS[COMP_CWORD-1]}
  local penult=${COMP_WORDS[COMP_CWORD-2]}

  # echo "penult=$penult pre=$prev cur=$cur" >> /tmp/smk.dbg
  # touch /tmp/smk.dbg; tail -f /tmp/smk.dbg

  [[ "$cur" == "" && "$prev" == "=" ]] && return 0

  if [[ "$prev" == "=" || "$cur" == "=" ]]; then
    local option=`[ "$prev" == "=" ] && echo $penult || echo $prev`
    [ "$cur" == "=" ] && cur=""
    case "$option" in
     --clean|--stable|--elf|--uboot|--xboot|--kernel|--system|--preset|--list|--product_configure_file)
        option="${option#--}"
        COMPREPLY=( $( compgen -W "`get_config_list $option`" -- $cur ) )
       ;;
    esac
    return 0
  fi

  local flag_preset="no"
  local flag_separate="no"
  local full_args=" \
      --clean --stable -f \
      --elf --elf= --uboot --uboot= --kernel --kernel= --system --system= \
      --ramdisk --preset= --new-board= -j --search= --list --list= --help \
      --product_configure_file --product_configure_file= "

  if [[ "$cur" == * ]]; then
    if (( $COMP_CWORD == 1 )); then
      COMPREPLY=( $( compgen -W "$full_args" -- $cur ) )
    else
      for i in "${COMP_WORDS[@]}"
      do
        [ "$i" == "$cur" ] && break
        case "$i" in
        --search|--list|--help) return ;;
        --clean|--stable|-f) full_args=`echo "$full_args" | sed "s/$i//"` ;;
        --preset) flag_preset="yes"; full_args=`echo "$full_args" | sed "s/--preset//"` ;;
        --new-board) full_args=`echo "$full_args" | sed "s/--new-board//"` ;;
        --elf) flag_separate="yes"; full_args=`echo "$full_args" | sed "s/--elf//g"` ;;
        --uboot) flag_separate="yes"; full_args=`echo "$full_args" | sed "s/--uboot//g"` ;;
        --kernel) flag_separate="yes"; full_args=`echo "$full_args" | sed "s/--kernel//g"` ;;
        --system) flag_separate="yes"; full_args=`echo "$full_args" | sed "s/--system//g"` ;;
        --ramdisk) flag_separate="yes"; full_args=`echo "$full_args" | sed "s/--ramdisk//"` ;;
        -j*) full_args=`echo "$full_args" | sed "s/-j//"` ;;
        esac
      done
      full_args=`echo "$full_args" | sed "s/--search//" | sed "s/--list//g" | sed "s/--help//"`
      [ "$flag_separate" == "yes" ] && full_args=`echo "$full_args" | sed "s/--preset//"`
      [ "$flag_preset" == "yes" ] && \
          full_args=`echo "$full_args" | sed "s/--uboot//g" | sed "s/--kernel//g" | sed "s/--system//g" | sed "s/--ramdisk//g"`
      full_args=`echo "$full_args" | sed "s/ =//g"`
      COMPREPLY=( $( compgen -W "$full_args" -- $cur ) )
    fi
  fi
  return 0
}
complete -F _smk smk

